require 'sparql/client'
class ApplicationController < ActionController::Base
  before_action :set_locale

  def set_locale
    if params[:locale].present? # explicit param overrides and sets session locale
      I18n.locale = params[:locale]
      session[:locale] = I18n.locale
    else
      I18n.locale = session[:locale] || I18n.default_locale
    end
  end

  @@sparql_headers = { 'User-Agent' => 'Ruby-Sparql-Client/1.0' }
  @@sparql_endpoint = SPARQL::Client.new("https://query.wikidata.org/sparql", headers: @@sparql_headers, read_timeout: 120)

  def find_lexemes_missing_usage_examples(lang, limit=100)
    begin
      lang = Rails.configuration.langs[lang][:language_qid]
      # Lexemes without usage examples
      query = "SELECT DISTINCT ?lexemeId ?lemma WHERE { ?lexemeId <http://purl.org/dc/terms/language> wd:#{lang}; wikibase:lemma ?lemma; ontolex:sense []. MINUS {?lexemeId p:P5831 [].} } LIMIT #{limit}"
      return @@sparql_endpoint.query(query) # returns a collection of RDF:QUERY::Solutions
    rescue
      return []
    end
  end
  def get_lexeme(lid)
    lid = lid.to_s # in case we were passed a raw L number
    lid = 'L'+lid unless lid[0] == 'L' # prepend L if got just number or number-as-string
    z = RestClient.get 'https://wikidata.org/w/api.php', {params: {action: 'wbgetentities', ids: lid, format: 'json'}}
    return JSON.parse(z.body)['entities'][lid]
  end
  #def lang_qid_by_iso_code(iso)
  #  @@sparql_endpoint.query("SELECT ?lang WHERE { ?lang wdt:P218 '#{iso}'. }").first[:lang].to_s.split('/').last
  #end
  def get_usage_examples(lexeme, lang)
    # get lemma
    lemma = lexeme['lemmas'].values.first['value'] # TODO: handle multiple lemmas

    # get usage examples from Wikisource
    res = RestClient.get "https://#{lang}.wikisource.org/w/api.php", {params: {action: 'query', list: 'search', srsearch: lemma, srwhat: 'text', srnamespace: 0, srlimit: 100, format: 'json'}} # TOOD: allow searching beyond the first 100 results
    return JSON.parse(res.body)['query']['search']
  end
  def invalidate_session
    session[:access_token] = nil
  end  
end
