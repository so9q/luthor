# Luthor

**Luthor** is a multi-lingual tool for adding **usage examples** to [lexemes](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data) on [Wikidata](https://wikidata.org), from sentences found on [Wikisource](https://wikisource.org) in the same language.

It is running [here on the Wikimedia Toolforge](https://luthor.toolforge.org) and, since it makes edits to Wikidata on the user's behalf (using [OAuth](https://en.wikipedia.org/wiki/OAuth)), requires users to have a (free) Wikimedia account.

To run it elsewhere, you would need to copy `config/constants.yml.example` to `config/constants.yml` and edit it to include your OAuth client ID and client secret. Other than that, it's a standard Rails app and should run after `bundle install`.

## Future plans
See the [TODO](TODO.md) file.

## Authors
See [AUTHORS.md](AUTHORS.md)
